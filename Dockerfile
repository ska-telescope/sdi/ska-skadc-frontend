FROM node:16.13.0-alpine

WORKDIR /home/node/app

COPY package.json package-lock.json ./
RUN npm install

COPY src src
COPY public public

CMD ["npm","start"]
