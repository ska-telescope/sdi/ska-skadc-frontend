NETWORK_MODE ?= host

DOCKER_COMPOSE_ARGS := NETWORK_MODE=$(NETWORK_MODE) 

PROJECT = ska-skadc-frontend

KUBE_NAMESPACE = ska-skadc

HELM_RELEASE = $(PROJECT)


# include makefile targets for Helm Charts management
-include .make/k8s.mk

# include makefile targets for Oci images management
-include .make/oci.mk

# include makefile targets for Helm management
-include .make/helm.mk

## The following should be standard includes
-include .make/base.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak


join-lint-reports: ## Join linting report (chart and npm)
	@echo -e "<testsuites>\n</testsuites>" > build/reports/linting.xml; \
	for FILE in build/reports/linting-*.xml; do \
	TEST_RESULTS=$$(tr -d "\n" < $${FILE} | \
	sed -e "s/.*<testsuites[^<]*\(.*\)<\/testsuites>.*/\1/"); \
	TT=$$(echo $${TEST_RESULTS} | sed 's/\//\\\//g'); \
	sed -i.x -e "/<\/testsuites>/ s/.*/$${TT}\n&/" build/reports/linting.xml; \
	rm -f build/reports/linting.xml.x; \
	done
