import React, { Component, Fragment } from 'react';
import { Provider } from 'react-redux';
import configureStore from './store';
import TreeParent from './components/TreeParent';
import InputSearch from './components/InputSearch';
import History from './components/History';
import './App.css';

const store = configureStore();

class App extends Component {

  render() {

    return (
      <div className='App'>
        <Provider store={store}>
          <Fragment>
            <div className='welcome_div'>
              <div className='welcome'>Welcome to</div>
              <div className='SKAO'> </div>
              <div className='welcome'>Dependencies Finder</div>
            </div>
            <InputSearch></InputSearch>
            <div style={{ height: '30px' }}></div>
            <History></History>
            <TreeParent></TreeParent>
            <div style={{ height: '30px' }}></div>
          </Fragment>
        </Provider>
      </div>
    );
  }
}

export default App;
