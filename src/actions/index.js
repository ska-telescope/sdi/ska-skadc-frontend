import { TASKS, TREE } from '../constants';

const loadTasks = (pattern) => ({
    type: TASKS.LOAD,
    pattern
});

const setTasks = Tasks => ({
    type: TASKS.LOAD_SUCCESS,
    Tasks,
});

const setTasksError = error => ({
    type: TASKS.LOAD_FAIL,
    error,
});

const loadTree = (pattern) => ({
    type: TREE.LOAD,
    pattern
});

const setTree = Tree => ({
    type: TREE.LOAD_SUCCESS,
    Tree,
});

const setTreeError = error => ({
    type: TREE.LOAD_FAIL,
    error,
});

export {
    loadTasks,
    setTasks,
    setTasksError,
    loadTree,
    setTree,
    setTreeError,
};