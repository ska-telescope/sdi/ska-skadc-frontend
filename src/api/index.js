async function queryTasks(pattern) {

  if (pattern) {
    try {
      const response = await fetch("/task/get/?artefact_name=" + encodeURIComponent(pattern));
      const fetchedTasks = await response.json();
      return fetchedTasks;
    }
    catch (ex) {
      console.log("Error on fetch: ", ex);
    }
  }
  else return [];
}

async function queryTree(pattern) {
  if (pattern) {
    try {
      const query = "/task/get_name_dependencies/?format=" + encodeURIComponent(pattern.format) 
      + "&artefact_name=" + encodeURIComponent(pattern.name) 
      + "&artefact_version=" + encodeURIComponent(pattern.version);
      const response = await fetch(query);
      const queryTree = await response.json();
      return { ...queryTree, save: pattern.save };
    }
    catch (ex) {
      console.log("Error on fetch: ", ex);
    }
  }
  else return [];
}

export { queryTasks, queryTree };