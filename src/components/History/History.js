import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loadTree } from '../../actions';
import { MDBBtn, MDBBtnGroup, MDBIcon } from "mdbreact";
import './History.css';

let currentPosition = 0;

class History extends Component {

    constructor(props) {
        super(props);
        this.handleClickBack = this.handleClickBack.bind(this);
    }

    handleClickBack() {
        this.setState({ goFoward: true });
        const { tree } = this.props;
        const { history } = tree;
        
        if(currentPosition>0)currentPosition--;
        else currentPosition = history.length-1;
        this.props.onLoadTree({ 
            name: history[currentPosition].name, 
            version: history[currentPosition].version, 
            format: history[currentPosition].format,
            save: false
        });
        
    }


    render() {
        const { tree } = this.props;
        const { history } = tree;
        
        if(history && tree.save===undefined) currentPosition = history.length-1;
        
        //console.log("history: ", history, " currentPosition: ",currentPosition, " tree: ",tree);

        return (
            history ?
            history.length > 1 ? 
            <div className='backDiv'>
                <MDBBtnGroup className="mr-2">
                    <MDBBtn
                        disabled={history ? history.length < 2 ? true : false : true}
                        onClick={this.handleClickBack}
                        color="pink lighten-2"
                        style={{color: 'white'}}
                    >
                        <MDBIcon icon="arrow-left" />
                    </MDBBtn>
                </MDBBtnGroup>
            </div>
            : null : null
        )
    }


}

const mapStateToProps = ({ tree, error }) => ({
    tree,
    error,
});

const mapDispatchToProps = dispatch => ({
    onLoadTree: (pattern) => dispatch(loadTree(pattern)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(History);