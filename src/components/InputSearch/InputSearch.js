import React, { Component } from 'react';
import { connect } from 'react-redux';
import { MDBInput, MDBBox } from "mdbreact";
import { loadTasks, loadTree } from '../../actions';
import './InputSearch.css';
import OCI from '../../assets/OCI.svg';
import HELM from '../../assets/helm.svg'
import PYTHON from '../../assets/python.svg'

class InputSearch extends Component {

  constructor(props) {
    super(props);
    this.state = {
      search: "",
      selection: ""
    }
    this.handleChange = this.handleChange.bind(this);
    this.clickOption = this.clickOption.bind(this);
  }

  parse_query_string(query) {
    var vars = query.split("&");
    var query_string = {};
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split("=");
      var key = decodeURIComponent(pair[0].replace("?",""));
      var value = decodeURIComponent(pair[1]);
      // If first entry with this name
      if (typeof query_string[key] === "undefined") {
        query_string[key] = decodeURIComponent(value);
        // If second entry with this name
      } else if (typeof query_string[key] === "string") {
        var arr = [query_string[key], decodeURIComponent(value)];
        query_string[key] = arr;
        // If third or later entry with this name
      } else {
        query_string[key].push(decodeURIComponent(value));
      }
    }
    return query_string;
  }

  componentDidMount(){
    if(window.location.search){
      const query = this.parse_query_string(window.location.search);
      if(query['format'] && query['artefact_name'] && query['artefact_version'])
        this.props.onLoadTree({ name: query['artefact_name'], version: query['artefact_version'], format: query['format'] });
    }
  }

  render() {
    const { tasks } = this.props;

    let obj = null;
    obj = tasks.map((element, k) => {
      return (

        <div key={k}
          className="myOptions"
          onClick={this.clickOption}
          data-name={element.name}
          data-version={element.version}
          data-format={element.format}

        >
          <div className='logoDiv'
            style={{
              backgroundImage: element.format === "OCI" ?
                `url(${OCI})` :
                element.format === "python" ?
                  `url(${PYTHON})`
                  : `url(${HELM})`
            }}
          >

          </div>
          <div className='nameDiv'
            data-name={element.name}
            data-version={element.version}
            data-format={element.format}
            value={element.name}
          >{element.name} : {element.version}
          </div>
        </div>
      )
    });

    const selection = this.state.search.length > 2 ?
      <div className="MySelectDiv">
        {obj}
      </div> : null;

    return (
      <React.Fragment>
        <MDBBox display="flex" justifyContent="center" >
          <MDBInput
            label="Search"
            icon="search"
            style={{ color: 'white' }}
            onChange={this.handleChange}
            value={this.state.search.length === 0 ? "" : this.value}
          />
        </MDBBox>
        {selection}
      </React.Fragment>
    )
  }

  handleChange(event) {
    const search = event.target.value;
    if (search.length > 2) {
      this.props.onLoadTasks(search);
    }
    this.setState({ search: search });
  }

  clickOption(event) {
    this.setState({ search: "", selection: event.target.innerHTML });
    this.props.onLoadTree({ name: event.target.dataset.name, version: event.target.dataset.version, format: event.target.dataset.format });
  }
}

const mapStateToProps = ({ tasks, error, tree }) => ({
  tasks,
  error,
  tree,
});

const mapDispatchToProps = dispatch => ({
  onLoadTasks: (pattern) => dispatch(loadTasks(pattern)),
  onLoadTree: (pattern) => dispatch(loadTree(pattern)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InputSearch);