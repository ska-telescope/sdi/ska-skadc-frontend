import React, { Component } from "react";
import { MDBContainer, MDBNotification, MDBAnimation } from "mdbreact";
import './Notification.css';

let timer = null;

class Notification extends Component {

  constructor() {
    super();
    this.state = {
        show: true,
    }
  }

  render() {

    const { title, text } = this.props;
    if(timer === null) timer = setInterval(() => { this.setState({show: false}); clearInterval(timer); timer=null; }, 2000);
    const date = new Date();
    const displayDate = "  "
    +date.getHours().toString()
    +':'+date.getMinutes().toString()
    +':'+date.getSeconds().toString()
   
    return (
      this.state.show &&
      <MDBAnimation type="zoomIn" duration="500ms"
      style={{
        position: 'fixed',
        top: '0',
        right: '0px',
        width: 'auto',
      }}>
        <MDBContainer>
          <MDBNotification
            icon="envelope"
            iconClassName="green-text"
            show
            fade
            title={title}
            message={text}
            text={displayDate}
          />
        </MDBContainer>
      </MDBAnimation>
    );
  }
}

export default Notification;