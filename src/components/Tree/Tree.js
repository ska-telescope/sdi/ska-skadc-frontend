import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loadTree } from '../../actions';
import './Tree.css';
import Notification from '../Notification';
import OCI from '../../assets/OCI.svg';
import HELM from '../../assets/helm.svg';
import PYTHON from '../../assets/python.svg';

class Tree extends Component {
    constructor() {
        super();
        this.state = {
            show: false,
            initialSetup: true
        }
        this.doubleClick = this.doubleClick.bind(this);
        this.noDependenciesClick = this.noDependenciesClick.bind(this);
        this.toggleView = this.toggleView.bind(this);
    }

    toggleView(show) {
        this.setState({
            show: !show,
            initialSetup: false,
            showNotitication: false,
            notificationTitle: '',
            notificationText: '',
        });
    }

    doubleClick(event) {
        event.preventDefault();
        let name = "";
        let version = 0;
        let format = "";
        if (event.target.childNodes[2]) {
            //click outside span
            name = event.target.childNodes[2].dataset.name;
            version = event.target.childNodes[2].dataset.version;
            format = event.target.childNodes[2].dataset.format;
        }
        else {
            //click inside span
            if (event.target.childNodes[0]) {
                name = event.target.dataset.name;
                version = event.target.dataset.version;
                format = event.target.dataset.format;
            }
        }
        if (name) {
            this.props.onLoadTree({ name, version, format });
        }
        else console.log("failname: ", name);
    }

    noDependenciesClick(event) {
        event.preventDefault();
        if (!this.state.showNotitication) {
            this.setState({
                showNotitication: true,
                notificationTitle: 'No more dependecies found',
                notificationText: 'No further dependencies were found',
            });
            setTimeout(() => this.setState({ showNotitication: false }), 2000);
        }
    }

    getStateVersion(node) {
        if(node.format=== "python" && node.version==="latest") 
        return { version: 0, label: 'Latest version should not be used, use a fixed version instead', icon: 'fa fa-exclamation-triangle deprecated' }
        if (node.deprecated) {
            return { version: 0, label: 'This version is deprecated', icon: 'fa fa-exclamation-triangle deprecated' }
        }
        else if (node.version === node.latest_version) {
            return { version: 1, label: 'This is using the latest version', icon: 'fa fa-check-circle uptodate' }
        }
        else if (node.name.indexOf('ska') === -1) {
            return { version: 2, label: 'This is a dependency external to SKA', icon: 'fa fa-check-circle uptodate' }
        }
        else {
            return { version: 3, label: 'There is a more recent version available "' + node.latest_version + '"', icon: 'fa fa-check-circle needupdate' }
        }
    }

    render() {

        const { node, level, onLoadTree, deprecated } = this.props;
        const { name, version, latest_version, format, dependencies } = node;

        if (!dependencies) return (<div></div>);

        const margin = 15;

        let show = this.state.show;
        if (this.state.initialSetup && level < 1 && !this.state.show) show = true;

        const treeRender = dependencies.map((node, key) => {
            if (node.dependencies.length === 0) {
                if(typeof(node.version)!== "string" && node.version.version) node.version = node.version.version;
                //No more depedencies
                return (
                    <div
                        className="Tree noselect"
                        style={{ marginLeft: String((level + 1) * margin) + "px" }}
                        onContextMenu={this.noDependenciesClick}
                        onDoubleClick={this.noDependenciesClick}
                        key={key}>
                        {/* names with NO Childs */}
                        <span className='deep_logo'
                            style={{
                                backgroundImage: node.format === "oci" ?
                                    `url(${OCI})`
                                    : node.format === "python" ?
                                        `url(${PYTHON})`
                                        : `url(${HELM})`
                            }}
                        ></span>
                        <span
                            className='name'
                            data-name={node.name}
                            data-version={node.version}
                            data-format={node.format}
                        >{node.name + " : " + node.version}</span>
                        <span
                            title={this.getStateVersion(node).label}
                            className={this.getStateVersion(node).icon}
                            data-state_version={this.getStateVersion(node).version}
                        > </span>
                    </div>
                )
            }
            else {
                //Recursive Tree
                return (
                    <Tree
                        key={key}
                        /* names with Childs */
                        node={node}
                        level={level + 1}
                        onLoadTree={onLoadTree}
                    >
                    </Tree>
                )
            }

        })
        const classSign = show ? "Sign fa fa-angle-right fa-rotate-90 down rotate" : "Sign fa fa-angle-right fa-rotate-0 rotate";
        const bold = level === 0 && "bold";
        return (
            <div>
                {this.state.showNotitication && <Notification
                    title={this.state.notificationTitle}
                    text={this.state.notificationText}
                ></Notification>
                }
                <div
                    className="Tree noselect"
                    style={{ marginLeft: String(level * margin) + "px" }}
                    onClick={() => this.toggleView(show)}
                    onContextMenu={this.doubleClick}
                    onDoubleClick={this.doubleClick}
                >
                    {/* Top tree render */}
                    <span className={classSign}></span>
                    <span className='deep_logo'
                        style={{
                            backgroundImage: format === "oci" ?
                                `url(${OCI})`
                                : format === "python" ?
                                    `url(${PYTHON})`
                                    : `url(${HELM})`
                        }}
                    ></span>
                    <span
                        className={bold + ' name'}
                        data-name={name}
                        data-version={version}
                        data-format={format}
                    >{name + " : " + version}</span>
                    {level !== 0 && <span
                        className={this.getStateVersion({ name, deprecated, latest_version, version, format }).icon}
                        title={this.getStateVersion({ name, deprecated, latest_version, version, format }).label}
                    > </span>}
                </div>
                {(this.state.initialSetup && level < 1) ? treeRender : show && treeRender}
            </div>
        )
    }
}

const mapStateToProps = ({ tree, error }) => ({
    tree,
    error,
});

const mapDispatchToProps = dispatch => ({
    onLoadTree: (pattern) => dispatch(loadTree(pattern)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Tree);
