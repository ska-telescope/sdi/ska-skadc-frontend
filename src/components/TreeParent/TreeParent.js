import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loadTree } from '../../actions';
import Tree from '../Tree';

class TreeParent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            hover: false
        }
        this.handleHover = this.handleHover.bind(this);
    }

    handleHover(hover) {
        hover ? this.setState({ hover: true }) : this.setState({ hover: false });
    }

    render() {

        const { tree } = this.props;

        if (Object.keys(tree).length === 0) return (null);

        const tip =  
        <div>
            <div className='tip noselect'>
                <span className='fa fa-exclamation-triangle deprecated'><span className='text'> The version is deprecated</span></span><br></br>
                <span className='fa fa-check-circle needupdate'><span className='text'> There is a more recent version available</span></span><br></br>
                <span className='fa fa-check-circle uptodate'><span className='text'> This is using the latest version</span></span>
            </div>
            <div className='tip noselect'>Right mouse click or Double click to search dependency</div>
        </div>;
        const node = { 
            name: tree.name, 
            format: tree.format, 
            version: tree.version, 
            latest_version: tree.latest_version, 
            deprecated: tree.deprecated,
            dependencies: tree.dependencies
        };
        return (
            <div className='center'
                onMouseEnter={() => this.handleHover(true)}
                onMouseLeave={() => this.handleHover(false)} >
                <Tree
                    node={node}
                    level={0}
                />
                {tip}
            </div>
        )
    }
}

const mapStateToProps = ({ tree, error }) => ({
    tree,
    error,
});

const mapDispatchToProps = dispatch => ({
    onLoadTree: (pattern) => dispatch(loadTree(pattern)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(TreeParent);
