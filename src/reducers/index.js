import { combineReducers } from 'redux';
import tasksReducer from './tasksReducer';
import treeReducer from './treeReducer';

const rootReducer = combineReducers({
    tasks: tasksReducer,
    tree: treeReducer,
})

export default rootReducer;