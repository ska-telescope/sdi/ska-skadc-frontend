import { TREE } from '../constants';

const treeReducer = (state = {}, action) => {
    if (action.type === TREE.LOAD_SUCCESS) {
        let tmp_history = state['history']; 
        state = action['Tree'];
        if(state.save === undefined){
            if(!tmp_history) tmp_history = [{
                name: action['Tree'].name,
                version: action['Tree'].version,
                format: action['Tree'].format
            }]
            else tmp_history.push({
                name: action['Tree'].name,
                version: action['Tree'].version,
                format: action['Tree'].format
            })
        }
        
        state['history'] = tmp_history;
        return state;
    }
    return state;
};

export default treeReducer;