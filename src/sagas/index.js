import { all } from 'redux-saga/effects';
import tasksSaga from './tasksSaga';
import treeSaga from './treeSaga';

export default function* rootSaga() {
    yield all([tasksSaga(),treeSaga()]);
}
