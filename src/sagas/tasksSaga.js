import { put, call, take, fork } from 'redux-saga/effects';
import { setTasks, setTasksError } from '../actions';
import { TASKS } from '../constants';
import { queryTasks } from '../api';

export function* handleTasksLoad(pattern) {
    try {
        const tasks = yield call(queryTasks,pattern);
        yield put(setTasks(tasks));
    } catch (error) {
        yield put(setTasksError(error.toString()));
    }
}

export default function* watchFunction() {
    while (true) {
        const { pattern } = yield take(TASKS.LOAD);
        yield fork(handleTasksLoad,pattern);
    }
}