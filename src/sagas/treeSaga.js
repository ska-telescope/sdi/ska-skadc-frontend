import { put, call, take, fork } from 'redux-saga/effects';
import { setTree, setTreeError } from '../actions';
import { TREE } from '../constants';
import { queryTree } from '../api';

export function* handleTreeLoad(pattern) {
    try {
        const Tree = yield call(queryTree,pattern);
        yield put(setTree(Tree));
    } catch (error) {
        yield put(setTreeError(error.toString()));
    }
}

export default function* watchFunction() {
    while (true) {
        const { pattern } = yield take(TREE.LOAD);
        yield fork(handleTreeLoad,pattern);
    }
}